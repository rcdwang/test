'''一个函数接受另一个函数作为参数传入，则前者函数即为高阶函数。
比如，把函数B作为参数传入函数A，则称，函数A为高阶函数
'''

def fa(x,y):
	print('函数fa传入的参数是{},{}'.format(x,y))
	y(x)
	return None

def fb(x):
	print('函数fb传入的参数是{}'.format(x))
	return None

fa(1,fb)

'''Map
map是一个内置函数，它接受2个参数，一个是函数，另一个是可迭代对象，把可迭代对象的元素依次传入第一个参数的函数中进行处理，最后返回一个Iterator'''


print(map.__module__)

def f(x):
	return x ** 2

m = map(f, range(1,11))

from collections.abc import Iterable, Iterator

print('map函数返回值m的类型是：', type(m))
print('map函数返回值m是map类的实例吗？', isinstance(m, map))
print('map函数返回值m是Iterable类的实例吗？' , isinstance(m, Iterable))
print('map函数返回值m是Iterator类的实例吗？' , isinstance(m, Iterator))

print(next(m))

print(next(m))

for i in m:
	print(i)

'''总结
高阶函数：
函数A 函数B，把函数B作为参数传入函数A，则称函数A为高阶函数。

高阶函数map
01 map是一个BIF，内置在builtins模块中。
02 map函数接受两个参数，一个是函数，另外一个是*Iterables
map(func, *iterables) --> map object
func对Iterable中的每一个数值进行处理，并返回一个值，最后构建出一个新的完整的Iterator.
03 map函数的返回值是一个map对象，同时它也是Iterable，也是Iterator
'''